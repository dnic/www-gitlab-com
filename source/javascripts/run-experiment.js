// Define the namespace
var runExperiment = function(issueId, featureFlag, experimentControlExtraFunctions, experimentTestExtraFunctions)
{
  // Define targets
  var experimentWrapperId = '#experiment' + issueId;
  var experimentTargetControl = experimentWrapperId + ' .experimentControl';
  var experimentTargetTest = experimentWrapperId + ' .experimentTest';
  var test = document.querySelector(experimentTargetTest);
  var control = document.querySelector(experimentTargetControl);

  // Initialize the user object
  var user = {'anonymous': true};
  // Initialize LaunchDarkly
  var ldclient = LDClient.initialize('5e3b01228b2fc906f60d9be2', user);

  // Define what should happen
  function render()
  {

    // Init the feature flag object
    var experimentIsEnabled = ldclient.variation(featureFlag, false);

    // Show, hide, and run extra functions.
    if(experimentIsEnabled)
    {
      // THIS IS THE TEST VARIANT

      // TIMEOUTS: wait 150 ms to poll experimentIsEnabled
      setTimeout(function()
      {
        document.querySelector(experimentTargetControl).classList.remove('experimentVisible');
        document.querySelector(experimentTargetTest).classList.add('experimentVisible');
      }, 150);

      if(experimentTestExtraFunctions !== null)
      {
        // TIMEOUTS: wait another 200 ms for the fade animation to finish (total 350ms)
        setTimeout(function()
        {
          experimentTestExtraFunctions();
          console.log('test extras ran');
        }, 350);
      };

    } else {
      // THIS IS THE CONTROL VARIANT

      // TIMEOUTS: wait 100 ms to poll experimentIsEnabled
      setTimeout(function()
      {
        document.querySelector(experimentTargetTest).classList.remove('experimentVisible');
        document.querySelector(experimentTargetControl).classList.add('experimentVisible');
      }, 150);

      if(experimentControlExtraFunctions !== null)
      {
        // TIMEOUTS: wait another 200 ms for the fade animation to finish (total 350ms)
        setTimeout(function()
        {
          experimentTestExtraFunctions();
          console.log('control extras ran');
        }, 350);
      };
    };
  };
  // Subscribe to the feature flag and then run the functions based on it's state
  ldclient.on('ready', render);
  ldclient.on('change', render);
};
